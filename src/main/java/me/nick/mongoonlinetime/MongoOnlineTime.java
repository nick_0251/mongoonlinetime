package me.nick.mongoonlinetime;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import com.qrakn.phoenix.lang.file.type.BasicConfigurationFile;
import lombok.Getter;
import me.nick.mongoonlinetime.profile.Profile;
import me.nick.mongoonlinetime.profile.ProfileListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public class MongoOnlineTime extends JavaPlugin {

    private static MongoOnlineTime mongoOnlineTime;

    @Getter
    private BasicConfigurationFile mainConfig;

    @Getter
    private MongoDatabase mongoDatabase;

    @Override
    public void onEnable() {
        mongoOnlineTime = this;

        mainConfig = new BasicConfigurationFile(this, "config");

        loadMongo();

        Profile.init();

        Arrays.asList(
                new ProfileListener()
        ).forEach(listener -> getServer().getPluginManager().registerEvents(listener, this));
    }

    @Override
    public void onDisable() {
    }

    private void loadMongo() {
        if (mainConfig.getBoolean("MONGO.AUTHENTICATION.ENABLED")) {
            mongoDatabase = new MongoClient(
                    new ServerAddress(
                            mainConfig.getString("MONGO.HOST"),
                            mainConfig.getInteger("MONGO.PORT")
                    ),
                    MongoCredential.createCredential(
                            mainConfig.getString("MONGO.AUTHENTICATION.USERNAME"),
                            "admin", mainConfig.getString("MONGO.AUTHENTICATION.PASSWORD").toCharArray()
                    ),
                    MongoClientOptions.builder().build()
            ).getDatabase(mainConfig.getString("MONGO.DATABASE"));
        } else {
            mongoDatabase = new MongoClient(mainConfig.getString("MONGO.HOST"), mainConfig.getInteger("MONGO.PORT"))
                    .getDatabase(mainConfig.getString("MONGO.DATABASE"));
        }
    }

    public static MongoOnlineTime get() {
        return mongoOnlineTime;
    }

}
