package me.nick.mongoonlinetime.profile;

import me.nick.mongoonlinetime.MongoOnlineTime;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;

public class ProfileListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoinEvent(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		if (p.hasPermission("mot.save")) {
			Profile profile = new Profile(p.getUniqueId());

			try {
				profile.load();
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}

			Profile.getProfiles().put(p.getUniqueId(), profile);

			profile.setName(p.getName());
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerQuitEvent(PlayerQuitEvent event) {
		Player p = event.getPlayer();
		if (p.hasPermission("mot.save")) {
			Profile profile = Profile.getProfiles().get(event.getPlayer().getUniqueId());

			new BukkitRunnable() {
				@Override
				public void run() {
					profile.save();
				}
			}.runTaskAsynchronously(MongoOnlineTime.get());
			Profile.getProfiles().remove(profile.getUuid());
		}
	}

}
