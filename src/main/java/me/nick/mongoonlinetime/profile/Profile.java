package me.nick.mongoonlinetime.profile;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import lombok.Getter;
import lombok.Setter;
import me.nick.mongoonlinetime.MongoOnlineTime;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Profile {

	@Getter private static Map<UUID, Profile> profiles = new HashMap<>();
	private static MongoCollection<Document> collection;

	@Getter private UUID uuid;
	@Getter @Setter String name;
	@Getter @Setter int time;

	public Profile(UUID uuid) {
		this.uuid = uuid;
	}

	public Player getPlayer() {
		return Bukkit.getPlayer(uuid);
	}

	public void load() {
		Document document = collection.find(Filters.eq("uuid", uuid.toString())).first();

		if (document == null) {
			this.save();
			return;
		}

		this.setTime(document.getInteger("time"));
	}

	public void save() {
		Document document = new Document();
		document.put("uuid", uuid.toString());
		document.put("name", name);
		document.put("time", time);

		collection.replaceOne(Filters.eq("uuid", uuid.toString()), document, new ReplaceOptions().upsert(true));
	}

	public static void init() {
		collection = MongoOnlineTime.get().getMongoDatabase().getCollection("profiles");

		// Players might have joined before the plugin finished loading
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (player.hasPermission("mot.save")) {
				Profile profile = new Profile(player.getUniqueId());

				try {
					profile.load();
				} catch (Exception e) {
					player.kickPlayer(ChatColor.RED + "The server is loading...");
					continue;
				}

				profiles.put(player.getUniqueId(), profile);
			}
		}

		// Save every minute to prevent data loss
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Profile profile : Profile.getProfiles().values()) {
					if (profile.getPlayer().hasPermission("mot.save")) {
						profile.setTime(profile.getTime() + 1);
						profile.save();
					}
				}
			}
		}.runTaskTimerAsynchronously(MongoOnlineTime.get(), 120L, 120L);
	}

	public static Profile getByUuid(UUID uuid) {
		Profile profile = profiles.get(uuid);

		if (profile == null) {
			profile = new Profile(uuid);
		}

		return profile;
	}

}
